package com.example.user.mytabs

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v4.view.ViewPager
import kotlinx.android.synthetic.main.activity_top_five.*
import java.util.*

class TopFiveActivity : AppCompatActivity() {
    private var imageModelArrayList: ArrayList<ImageModel>? = null

    private val myImageList = arrayListOf<String>(
        "http://www.realdetroitweekly.com/wp-content/uploads/2017/06/Restaurants.jpg",
        "http://www.realdetroitweekly.com/wp-content/uploads/2017/06/Restaurants.jpg",
        "http://www.realdetroitweekly.com/wp-content/uploads/2017/06/Restaurants.jpg",
        "http://www.realdetroitweekly.com/wp-content/uploads/2017/06/Restaurants.jpg",
        "https://static.standard.co.uk/s3fs-public/thumbnails/image/2016/06/14/06/the-clove-club.jpg?w968"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_top_five)

        imageModelArrayList = ArrayList()
        imageModelArrayList = populateList()

        init()

    }

    private fun populateList(): ArrayList<ImageModel> {

        val list = ArrayList<ImageModel>()

        for (image in myImageList) {
            val imageModel = ImageModel(image)
            list.add(imageModel)
        }
        return list
    }

    private fun init() {

        mViewPager.adapter = TopFiveSlidingAdapter(this@TopFiveActivity, this.imageModelArrayList!!)

        mIndicator.setViewPager(mViewPager)
        val density = resources.displayMetrics.density

        mIndicator.setRadius(5 * density)
        var NUM_PAGES = imageModelArrayList!!.size
        var currentPage: Int = 0
        // Auto start of viewpager
        val handler = Handler()
        val Update = Runnable {
            if (currentPage == NUM_PAGES) {
                currentPage = 0
            }
            mViewPager!!.setCurrentItem(currentPage++, true)
        }
        val swipeTimer = Timer()
        swipeTimer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(Update)
            }
        }, 5000, 5000)

        // Pager listener over indicator
        mIndicator.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageSelected(position: Int) {
                currentPage = position

            }

            override fun onPageScrolled(pos: Int, arg1: Float, arg2: Int) {

            }

            override fun onPageScrollStateChanged(pos: Int) {
            }
        })

    }
}


