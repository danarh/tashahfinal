package com.example.user.mytabs

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        tNext.setOnClickListener({
            var intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)

        })
    }
}
