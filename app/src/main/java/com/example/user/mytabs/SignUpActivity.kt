package com.example.user.mytabs

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.android.gms.tasks.OnFailureListener
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        bSubmit.setOnClickListener({
            submitClickAction()

        })
    }
    fun submitClickAction() {
        eName.error = null
        eEmaill.error = null
        ePass.error = null
        eConfirmPassword.error = null

        var name = eName.text.toString()
        var email = eEmaill.text.toString()
        var pass = ePass.text.toString()
        var cPass = eConfirmPassword.text.toString()

        if (validation()) {

            var mAuth: FirebaseAuth = FirebaseAuth.getInstance();

            mAuth.createUserWithEmailAndPassword(eEmaill.text.toString(), ePass.text.toString()).addOnCompleteListener(this,
                { it ->
                    if (it.isSuccessful) {
                        EmailVar()
                        Toast.makeText(this,eEmaill.text.toString(), Toast.LENGTH_SHORT).show()

                        var intent = Intent(this, VerificationActivity::class.java)
                        startActivity(intent)

                    } else {
                        Toast.makeText(this, it.exception!!.localizedMessage, Toast.LENGTH_SHORT).show()
                    }

                }).addOnFailureListener(this, OnFailureListener {
                Toast.makeText(this, it.message + eEmaill.text.toString(), Toast.LENGTH_SHORT).show()
            })


        }//if

    }
    fun validation(): Boolean{

        var name = eName.text.toString()
        var email = eEmaill.text.toString()
        var pass = ePass.text.toString()
        var cPass = eConfirmPassword.text.toString()
        var valid: Boolean = true


        if (name.isEmpty()) {
            eName.error = "Please check your name"
            valid = false
        }
        if (email.isEmpty()) {
            eEmaill.error = "Please check your E-mail"
            valid = false
        }
        if (pass.isEmpty()) {
            ePass.error = "Please check your password"
            valid = false
        }
        if (cPass.isEmpty()) {
            eConfirmPassword.error = "Please check your password"
            valid = false
        }
        if (!pass.equals(cPass)) {
            eConfirmPassword.error = "Password doesn't match"
            valid = false
        }
        return valid
    }

    fun EmailVar(){
        val user= FirebaseAuth.getInstance().currentUser
        user?.sendEmailVerification()?.addOnCompleteListener { Toast.makeText(this,"Verification email sent", Toast.LENGTH_SHORT).show()}

    }
}

